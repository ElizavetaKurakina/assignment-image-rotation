#pragma once
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#pragma pack(push, 1)
struct bmpHeader 


#pragma pack(pop)

struct pixel
{
    uint8_t b, g, r;
};

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_NAME,
    READ_SIZE_LIMIT,
    READ_ERROR
};

enum write_status
{
    WRITE_OK = 0,
    WRITE_INVALID_NAME,
    WRITE_NO_IMAGE, 
    WRITE_ERROR
};

enum read_status fromBmp(FILE*, struct image*);
enum write_status toBmp(FILE*, struct image const*);
